# frozen_string_literal: true

# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# coding:utf-8

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'comap/version'

Gem::Specification.new do |spec|
  spec.name          = 'comap'
  spec.version       = COMAP::VERSION
  spec.authors       = ['Sylvain Arrambourg', 'Samuel Bernard']
  spec.email         = ['incoming+sre-gems/comap@incoming.gitlab.com']
  spec.license       = 'Apache-2.0'

  spec.summary       =
    'Container Orchestrator Metrics Aggregator for Prometheus'
  spec.description   =
    'Container Orchestrator Metrics Aggregator for Prometheus'
  spec.homepage      = 'https://gitlab.com/sre-gems/comap'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 12.2'
  spec.add_development_dependency 'rspec', '~> 3'
  spec.add_development_dependency 'rubocop', '~> 0.51'
  spec.add_development_dependency 'webmock', '~> 2.0'

  spec.add_dependency 'faraday', '~> 0.13'
  spec.add_dependency 'json', '~> 2.0'
  spec.add_dependency 'thin', '~> 1.7'
end
