# frozen_string_literal: true

#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

def wait_service(name, replica)
  (1..10).each do |try|
    result = `docker service ls --format='{{.Name}} {{.Replicas}}' 2>&1`
    break if result.include?("#{name} #{replica}/#{replica}")

    puts "Waiting for #{name} to be up with #{replica} replicas [#{try}/10]"
    sleep(5)
  end
end

{
  'replicated' => '5678',
  'global' => '5679'
}.each_pair do |mode, port|
  describe "Metrics service in #{mode}" do
    replicas = mode.eql?('global') ? 1 : 2
    wait_service("metrics-#{mode}", replicas)

    it 'should run' do
      result = `docker service ls 2>&1`
      expect(result).to include('metrics', "#{replicas}/#{replicas}")
    end

    it 'should return metrics from one node' do
      result = <<-RESULT.gsub(/^ */, '')
        # TYPE metrics_sample_seconds gauge
        metrics_sample_seconds 42
        # TYPE metrics_sample_2_seconds gauge
        metrics_sample_2_seconds 24
      RESULT
      expect(command("curl 127.0.0.1:#{port}").stdout).to eq(result)
    end
  end
end

service = 'service="metrics-replicated"'
result_replicated = <<-RESULT.gsub(/^ */, '')
  # TYPE metrics_sample_seconds gauge
  metrics_sample_seconds{#{service},container="metrics-replicated.1"} 42
  metrics_sample_seconds{#{service},container="metrics-replicated.2"} 42
  # TYPE metrics_sample_2_seconds gauge
  metrics_sample_2_seconds{#{service},container="metrics-replicated.1"} 24
  metrics_sample_2_seconds{#{service},container="metrics-replicated.2"} 24
RESULT

worker_cmd =
  'docker node ls --filter '\
  'name=comap-swarm-worker-centos-7 --format \'{{.ID}}\''
worker = `#{worker_cmd}`.chomp
service = 'service="metrics-global"'
result_global = <<-RESULT.gsub(/^ */, '')
  # TYPE metrics_sample_seconds gauge
  metrics_sample_seconds{#{service},container="metrics-global.#{worker}"} 42
  # TYPE metrics_sample_2_seconds gauge
  metrics_sample_2_seconds{#{service},container="metrics-global.#{worker}"} 24
RESULT

{
  'replicated' => { 'result' => result_replicated, 'port' => '9397' },
  'global' => { 'result' => result_global, 'port' => '9398' }
}.each_pair do |mode, test|
  describe "Comap service aggregating metrics from #{mode} service" do
    wait_service("comap-#{mode}", 1)

    it 'should run' do
      result = `docker service ls 2>&1`
      expect(result).to include("comap-#{mode}", '1/1')
    end

    cmd = "curl 127.0.0.1:#{test['port']}"
    it 'should return metrics from both nodes' do
      expect(command(cmd).stdout).to eq(test['result'])
    end
  end
end
