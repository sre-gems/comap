# frozen_string_literal: true

# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'faraday'
require 'json'

# Module Docker
module COMAP
  # Simple Engine API client
  class DockerClient
    def initialize(url, port, ssl = {})
      @conn = Faraday.new(url: "#{url}:#{port}", ssl: ssl) do |faraday|
        faraday.options.params_encoder = Faraday::FlatParamsEncoder
        faraday.adapter Faraday.default_adapter
      end
    end

    def call(route, filters = {})
      response = @conn.get(route) do |req|
        req.params['filters'] = filters.to_json
      end
      JSON.parse(response.body)
    rescue StandardError => e
      puts "Could not connect to Docker API #{@conn.url_prefix}"
      puts e.message
    end
  end
end
