# frozen_string_literal: true

# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

id = 'jf1rxu9p5eh3lq76nlh4rhig1'
tasks = (1..3).map do |i|
  {
    'Spec' => { 'Networks' => [{ 'Target' => 'hxb6ii8jpyczfd343b1sv9c2s' }] },
    i != 2 ? 'Slot' : 'NodeID' => i != 2 ? i.to_s : id,
    'DesiredState' => 'running',
    'Status' => { 'State' => i != 3 ? 'running' : 'failed' },
    'NetworksAttachments' => [{
      'Network' => {
        'ID' => 'hxb6ii8jpyczfd343b1sv9c2s',
        'Spec' => { 'Name' => 'network1' }
      },
      'Addresses' => ["10.0.0.#{i}/24"]
    }, {
      'Network' => {
        'ID' => 'hxb6ii8jpyczfd343b1sv9c2s',
        'Spec' => { 'Name' => 'network2' }
      },
      'Addresses' => ["10.255.0.#{i}/24"]
    }]
  }
end

version = { 'ApiVersion' => '1.29' }

RSpec.configure do |config|
  config.before(:each) do
    url = 'http://127.0.0.1'
    good_port = '2375'
    bad_port  = '3000'

    # TODO: add empty tasks test case
    filters = {
      'service' => { 'service1' => true },
      'desired-state' => { 'running' => true }
    }
    params = "filters=#{CGI.escape(filters.to_json)}"

    stub_request(:get, "#{url}:#{good_port}/tasks?#{params}")
      .to_return(status: 200, body: tasks.to_json, headers: {})

    filters['service'] = { 'service2' => true }
    params = "filters=#{CGI.escape(filters.to_json)}"

    stub_request(:get, "#{url}:#{good_port}/tasks?#{params}")
      .to_return(status: 200, body: {}.to_json, headers: {})

    filters = {}
    params = "filters=#{CGI.escape(filters.to_json)}"

    stub_request(:get, "#{url}:#{good_port}/version?#{params}")
      .to_return(status: 200, body: version.to_json, headers: {})
    stub_request(:get, "#{url}:#{bad_port}/version?#{params}")
      .to_return(status: 404, body: nil, headers: {})
  end
end
