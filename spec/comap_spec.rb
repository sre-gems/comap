# frozen_string_literal: true

# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

service = 'service="service1"'
container_r = 'container="service1.1"'
container_g = 'container="service1.jf1rxu9p5eh3lq76nlh4rhig1"'
aggregated_metrics = <<~BODY
  # TYPE metrics_sample_seconds gauge
  metrics_sample_seconds{#{service},#{container_r}} 1
  metrics_sample_seconds{#{service},#{container_g}} 2
  # TYPE metrics_timestamped_sample_seconds gauge
  metrics_timestamped_sample_seconds{#{service},#{container_r}} 1 1518528131
  metrics_timestamped_sample_seconds{#{service},#{container_g}} 2 1518528132
  # TYPE metrics_labeled_sample_seconds gauge
  metrics_labeled_sample_seconds{label="test1",#{service},#{container_r}} 1
  metrics_labeled_sample_seconds{label="test2",#{service},#{container_g}} 2
  # TYPE metrics_labeled_with_space_sample_seconds gauge
  metrics_labeled_sample_seconds{label="test 1",#{service},#{container_r}} 1
  metrics_labeled_sample_seconds{label="test 2",#{service},#{container_g}} 2
BODY

RSpec.describe COMAP do
  it 'has a version number' do
    expect(COMAP::VERSION).not_to be nil
  end
end

# rubocop:disable Metrics/BlockLength
RSpec.describe COMAP::App do
  context 'when docker is not reachable at launch' do
    it 'should raise connection error' do
      port = '3000'
      expect do
        COMAP::App.new(docker_port: port)
      end.to raise_error(StandardError)
    end
  end

  ['', 'metrics'].each do |path|
    context "when get to #{path}" do
      services = %w[service1:4000 service2]

      let(:app) do
        COMAP::App.new(services: services,
                       network: 'network1',
                       docker_ssl: 'verify=false',
                       metrics_path: path)
      end
      let(:env) { { 'REQUEST_METHOD' => 'GET', 'PATH_INFO' => path } }
      let(:response) { app.call(env) }
      let(:status)   { response[0] }
      let(:body)     { response[2][0] }

      it 'returns the status 200' do
        expect(status).to eq '200'
      end

      it 'returns aggregated metrics' do
        expect(body).to eq aggregated_metrics
      end
    end
  end
  # rubocop:enable Metrics/BlockLength
end
