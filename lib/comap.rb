# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'bundler/setup'
require 'optparse'
require 'rack'
require 'logger'
require 'comap'

# Main Module
module COMAP
  class << self
    Dir[File.join(File.dirname(__FILE__), '*', '*.rb')].each do |file|
      require file
    end

    def add_docker_host_option(opts)
      desc = "Docker Engine API Host (default: #{@config[:docker_host]})"
      help = '--docker_host HOST'
      opts.on('-H', help, desc) { |v| @config[:docker_host] = v }
    end

    # Merge port with -H (to be coherent with docker)
    def add_docker_port_option(opts)
      desc = "Docker Engine API Port (default: #{@config[:docker_port]})"
      help = '--docker_port PORT'
      opts.on('-p', help, desc) { |v| @config[:docker_port] = v }
    end

    def add_docker_ssl_option(opts)
      desc = 'Docker Engine API SSL (default: none)'
      help = '--docker_ssl k1=v1,k2=v2'
      opts.on('-s', help, desc) { |v| @config[:docker_ssl] = v }
    end

    def add_network_option(opts)
      desc = 'Docker swarm service network (default: none)'
      help = '--docker_network NETWORK'
      opts.on('-n', help, desc) { |v| @config[:network] = v }
    end

    def add_metrics_path_option(opts)
      desc = "Metrics path (default: #{@config[:metrics_path]})"
      help = '--metrics_path METRICS_PATH'
      opts.on('-m', help, desc) { |v| @config[:metrics_path] = v }
    end

    def add_options(opts)
      opts.banner =
        "Usage: #{File.basename($PROGRAM_NAME)} [options] swarm_service"
      add_docker_host_option(opts)
      add_docker_port_option(opts)
      add_docker_ssl_option(opts)
      add_network_option(opts)
      add_metrics_path_option(opts)
    end

    def parse_opts
      OptionParser.new do |opts|
        add_options(opts)
        opts.on('-h', '--help', 'Display this screen') do
          puts opts
          exit
        end
        opts.parse!
      end
    end

    def app
      config = @config
      Rack::Builder.new do
        use Rack::Deflater
        run COMAP::App.new(config)
      end
    end

    def start(args = ARGV)
      @config = {
        docker_host: 'http://127.0.0.1',
        docker_port: 2375,
        docker_ssl: '',
        metrics_path: '',
        network: '',
        services: args
      }
      parse_opts
      Rack::Server.start(app: app, Host: '0.0.0.0', Port: 9397)
    end
  end
end
