# rubocop:disable all
{
  "id" => "docker-platform-kitchen",
  "ca" => {
    "encrypted_data" =>
      "47uX/XUJunyJTeEUrnS7nLW18cMju6qky3PDfe1H9swz9lXZW9p+1m7cUapi\n" \
      "yzvEfPMzMY8OjZjCC0YhA+3Pg/vReRU2IMGy6jsAltkz0ctz3jf2E3nZamHK\n" \
      "ZHidB2QqoyGxB+jroegSGQ8Vv1AA9Cfre5v8bzsp61ZBhPsNiUDoB27doMIx\n" \
      "fbPFpes7yL+hs3pthyA0R4E9cvcoKeQM3qyv7Z+vWDg3ANqBqjyeihl9Cw/J\n" \
      "9lYGBvaxtV2PANPHNQqATBZuwTiSZImrdlCQrws1jK5xNxhy+EpRjM2ShNoJ\n" \
      "/2Q1ADTOohPAw02spLftxZkmOz2vfaTGR6Od9BLbt1Mb3uIOK6do93rnF5S0\n" \
      "J6Tv0tW+xnNE3YUadbXBmy3DidBVcNYqze7q4cQNnnPVh+h82AI2OM1sTqJ4\n" \
      "eEI2+ysOj4PhCUMt7zd7XCyV4ZUXJNO5I8rgG42M4eOMYUxk96yHSlTAJD9g\n" \
      "7cyXjkkecsx2EEGsLQlunuGGm4Qcm4h2/xRLheunSG8VQLA6NH1W++Ppjt3U\n" \
      "2QCPk5NhbUXN0nYyREn1goMav3Es+FEjuLmcZM79MWM9jyXg83M2zMgueUXo\n" \
      "dJS/Dr3zadrUyxfa+Cvx8ILWNlrpZXTsay2yNQB3JuOrIh+w1BH64IpweWQ6\n" \
      "NMZbuNzqBrechkSKQc051MR73eXDfdqzaTnLUXrlb1tpyiUeMtj/NQGud4yY\n" \
      "InkxTUuaSXZjefaowS07zv1sIYxd5VhdFsmixp/O1zwvW0ScLI/6Piye7yYe\n" \
      "YdYxyf+GC2lmm09SVU2yo84DeZwQHHcIu4Xp8JBNRjZmIKijfaMzITerWKXu\n" \
      "D1vqtjlDcdi3d7NfG9MMqLbWlxMLllI+OGoloV2qGzDMoHrKIRGItQh0Cmk4\n" \
      "mDymhTlW1TqxuIm01lbePxPe2Tj0mzJMlb56Eu7V2/won360TJ3vLV4SlkCO\n" \
      "FG3SnMcVDTewtsjhgS/4u4I61O+T/dPCkPn2GicMjD0Lya/tfsdEtJc9qatp\n" \
      "aR4o+j91YixXxANL+gQESSF/XmcflEccDn/3U9i4+PfWnfU3j+Tlz1ShnYHc\n" \
      "40ISp3xF7WbOPtnF3xBEQsE3GTjrCDuyYB731Nuyh3Tzy8Y32sJdimLEMHZL\n" \
      "/+0wCdXn5LBM/pxjFOcmHPpUNrZhILwqYjT2vBVlaeeoCsecYhi6TqJzT9Sr\n" \
      "VNpfiRQKcSIiXtO4hMQrEV2rE50yigp63MFwMqfTLvdatUvLinsDGDMN2umu\n" \
      "YP7LJAsNAaNJLMjL4A6wjjcrmEu0Kpe1JElmvye3jamqJvovTAYkuBHGw70W\n" \
      "XJDlk3mXY1VP8Fr2kJgLor3ln5HHvROzsvVmemHnTwk3GqEnYntRi1FMonyX\n" \
      "Dxpps1OWQDtU3g9OiZgcoGnqrfWKSJrQCp8qrJKBHsXXJ2CksuyKNZPyXv4u\n" \
      "qxzTtRafP+rQlMO4hmA7vvnAdXVV1QmgzCkoIUA933jj7gtQ1yURWKNANCpk\n" \
      "n+X8g5bnYeJ7/5L8uBblh2tR4S69Lf53E6hxs3eT5UtCgPsAemPkNpYvdVJu\n" \
      "DfazksAiAod7InWKN9lr294lUggpKtrAD27fjyTvUDhPoG6cJyquzhsyIUPh\n" \
      "f2TXtYuWQTqImUMExsFOIlg9LhYzObw6l/2jK15YqYrSNLoaNKblvYVFI+Xv\n" \
      "3G3YWWRUpsRv5lLt5J3u2P+Yu1xaa87BwE09rzz0meNu0sI1dyUxHsxs4zDk\n" \
      "aST6tKldmyYlDlwDaT+qAVVGF9YHKPYLtyfqaMhFrjn69Y5hjK00JpojK8Ag\n" \
      "GvZat6BgeBDN0jQp8wFRvHoavxj9cnClsCaUlPIynrtu1ZyPK687lweBqGl/\n" \
      "1iaop/wCuhAZqBPd6vXx1BXfZcoGYVKStO04MKeX4RqY2TzONzXisDa/8dNZ\n" \
      "677WBpRSgbrcll+qnDDJtSeVVfQYc3IPea18lmTuP/NS7PxWj9IIPQU9VxHn\n" \
      "Z7+ozV3qHzCEXp8lMZDv1L14CVsQhcdi/c0T2+z5rPd3NyS/0+ZWFxvvIClI\n" \
      "yzbsxolP824kBPysGxKTRFAUFx2Vqnube0XGDq+aNyPEDJWdD2Y6wg2vIpcE\n" \
      "tF2bW+aZdSTq/QXUej4RGtYIMCte+Yx4d7SG/eWaGrPba5cwuS7kLga5CIoC\n" \
      "FPlcqC4V0JAXT/I/k+2KNFKpt9074BzUegQjCSVbU8eE8G68Qq7iSaNnNFUW\n" \
      "i8GBU5puG69JsnoxKFbY2wUHIsrDq2lDGbwmp5IyP1SNC/QlQAddT/wdt5ps\n" \
      "SrRiWVo082Ve5QFkuApVnXMcWUbKG/fwiV8R5QeDT3pbSQwG+D+u6K/XMLG1\n" \
      "PrWsxH9oXN7jLMSujfI7mqPossXjMxCzaqNopO0DTsYnn8HqbwR8TXVe79q+\n" \
      "c8NhEUDe2BkPI7zQVKIm4j0q8L20x2SuyfTzh3xJMPc8zrlbNjgT2rHAzJcT\n" \
      "e8CS7Ni3HppeC2lERt4uQzLSs5MP+24lKB3Cx8S/+MTDpbvFt97iuH9QCGoM\n" \
      "1PNXiOfF5nY1lkJea2zHPRcdyHzXNcTuzcyuqXTeHxyUbgjHW0BeJzZs6d1T\n" \
      "tOYMcD4SVlOZ/CSmLi1dEGZrYCHsVeiNCz/e/lpOZU60Ac6PuLD3ZUi5FNZ3\n" \
      "+B7yLVD4h/1g8fvz2c1FsTvmuI8Jc6/xhmSVBcCuqGJslRd03pDPC7yzbrk7\n" \
      "muzKA+fZvKLwtYIbownRcu792ODqjbQWfPQlKJcQSuwhnXtUKeO9POeNBOe9\n" \
      "pVCG8mFc8A+rODxSggrhTi/bqLdRZ+9BXGE5DkaxMUjb\n",
   "iv" => "enBtD3ayV353n3//\n",
   "auth_tag" => "HZwFqh2sBOvk2Xqxoq3dEQ==\n",
   "version" => 3,
   "cipher" => "aes-256-gcm",
  },
  "cert": {
    "encrypted_data":
     "nqHxZ+kEgDQHXLe+8r1uU5FGk4I4rAX8cOlWo7uVYoHSq/EABRuyQ6/PUyco\n" \
     "sb5JjOKD4jT+PfkOTVl32pb7+k3jWzQmwY5axBy6bhecAnEsnhE2gK7d4a0B\n" \
     "K0wpuXglReigbYyemod15nDnbVjRV5NpnjH/UJRzeIOte6ewJIG2DfSI1j1O\n" \
     "U9+2+bv6aaqTht0vnGGCY+CMBtXA7jNHjUcOZjbYW+a6J92+VZZvT965zCZg\n" \
     "5wAlS1MpsuoDzytvZshyrwax8MHO7UmXR/LaETFBxuncYZhNCa7guEPVjF3W\n" \
     "CAzQk8LNsTA0giHEYebWrLdUOOC+OtDt4qkB+BSCg6ce4FLUZFsTqnLXDyCp\n" \
     "DOmtK9JYZ2eHUcWqp7/t9+Pss9uYcNvyXScpkVYzxsVcv8cOQIfUhMZ9YrUC\n" \
     "ZF5c7bQOIOIkmeKFWtDzAEjiMiTQtGGOv57JphwqbZnzVjD6mT3dliZj31yn\n" \
     "LvX7//swYHMVwORfYgbhDxuN35Nwj/xdQ8u5eDhb1MfxT1x3VZlqdBtAQkR8\n" \
     "aWsDTdatzd1QgLw8Wq1loUdeHJq5cy9d6mgZJeZLy+12Lh3YWOC1JoIxP4hV\n" \
     "53/HaJVUj+UzFNcJGv4RWu14aRTvfCW94S7TwMlMwOQFQb4k0dTlkmNLYdiU\n" \
     "CDeIzz6ZyIh3bzVNJbA7+x6EZxSj6sslGVW1VJfXS8YvKpQcINT4wof+9jba\n" \
     "farHyXIULoAYryHd9G3Bu1VYC3cqVm38RlQLuxyB+uFHTWWQ9n5OvP+Pf6YL\n" \
     "7cEs/i8wawtm2iRy5tKPX4CpHlLKaSzLQM72YXprqTzq8GIyGfUF/tYAHlIm\n" \
     "wJu8YdnLkk/bJZ9yZPRwD3Ee92GEELl7x7cI9k77nN6KOggmjVum+CH1pueT\n" \
     "vPTKkD7cphSzDVxl3TZBLcPRL7gGN6o9/PPE1IgzncArtoNj8L0ak4BiK2pm\n" \
     "zBAEsG3vmNFIi0ogpiD2cSWgsDwJT4krCMb/z0FEC3elFthORg7PQXmQsj/H\n" \
     "AfqtsrBk8kj0/aCVAXQ9egHcR07xlwntUpiu8gunRQqtTdmpF96RO3cI3LAs\n" \
     "XgHQ/yGfIeOf4kzxLZJutLbKt6x0IIYfIXUmXn938mwwbKD5uYGwH3qS09xc\n" \
     "KXNxN9supsCjvpdzHt4lhZQ7vaOjes5KNVyPvbjEoGUQv5G/ZxkEbnkokiuQ\n" \
     "7N/v0LEfqNGWFJU+U8IHvtcftaUkNVWamp9S02+PQ1xYgw0WRZZpEuIahR8e\n" \
     "mvhFDNlc1jMzOdYHsS5w0DsZppdMn5Qy63zZFzC21jVNKrCmj4Vg1y2K6JCH\n" \
     "JTQGjU4bq9gYv5W4uCLvyKqzypzdBBTX9jMCaWkZlsRS5hiKvLzX1wt6PW7M\n" \
     "fH6c7uInqhAZLkbr86lGmaTh6qRao06rfFVjRTQVsuCEYzkAvzx5d7OVzoPW\n" \
     "x/5vQJMQGWJMnwVPIKppz/IbPCZWOfMEGyr2oM1E9o4uPfYjwOoG+z2aQyqB\n" \
     "8n8E1e4EMW7q4Q4BCTiFEp36zyMRefJvfhuyQyY0S7U3WGTLOkVRedqh/pLK\n" \
     "gg5ZNqqPcEHDGvDYZ9ZKxI7ee76Q6wY1SUyxLxjAiknxw4qRDZAh1P886etD\n" \
     "kotHC810Nz+G6Z+ZA6LC8Jc9txKgqcGppCaXNuU3VtUKVXbtqw9AjQwbR6at\n" \
     "H3nQyvL+xA+1his3qjHkOdBozJwbgUNSU+c1naab3kuqNn4RSrBH9Et4WJ8V\n" \
     "8A1j52LcSCxEUva5/ELz5Z3hIlf90ndUbbf1tRM7r+n3+KMjBFs+BMZzsRck\n" \
     "CiMwsASSxin9HqKWI5jVPodJT+f0OrryfOaRVaeSsr6nKA9gTt+TMsLhDs58\n" \
     "o6WL/7W4cm9JBntKvWaE9Pta++zj/LiwPv9Yma2klLNih1dtlIz0lRbBPgrO\n" \
     "jLP4Popg6yDCoO0KrXsxmBNNEy/46E5GEnM7U3KedYDVUSMb7yABY4bjlwCl\n" \
     "e2ya5xcXd/PGRAk/TUrXu/MClBNpdx1uDBOPJBYxm1rowMK7q1KKt64v9qcv\n" \
     "jU54PcGFmjJWzSHGxTDtTwWRGlDVgyxOKrsQkH/WCDE2MkIhWMzMfLdJSHqD\n" \
     "RcYAsKk5WoN+JyXRnawu+HyjlXs9S1lcvyGOct1e39JTsHSwXubgUWp5Mwb2\n" \
     "PTW5EA+wL4HWKCLnwXGIMD3FOOEDAu5YhtDYXycnJ5kxTk7czsqA29OGAvPO\n" \
     "Xwi4VA1vvHEcWL8tIfSjFLK5q+bqurssv91dBylLSKnOb5qSrOQ9bWpSf49N\n" \
     "jBrnYEwUGZmyU+F6e57FNkNFhSsXsKh8N3Y6djgmoUmtyCn9weUHqxevxP8T\n" \
     "IPgoNFFCmRZxLttQ1umSuBHqkLjwqqAdUIUc+gCHUxwngYE8Q2Ux6aTQyC5/\n" \
     "c+Jz6sfQdcelKKilDriqt3mjq6vETuI1M1IVQ5mw+x36UUICqODh49Yd6LG5\n" \
     "Yy/TW1hk+3+hjPRzUqzizzQXIGJ8qZP0kwUGh6/7JN6Xs9XOcF/WowDSLm0/\n" \
     "Q/b0kEKov4zWPxHHWNMboZ1rQdDdINRnVMqxtnYsKB9RcgAfUeCOcDq103vU\n" \
     "1EykpqNf1K+2/FGVlnAM5Q1zBEJ1l3uZg+hfEPBH7fbwphsnHLgQKkfig2T4\n" \
     "JfYhiVJ6F++NJ66vAf5sXd3pbLM0erKCZViiAdMlH+AsRl2PL1JoTs9fVIdv\n" \
     "8mQorhfXSjn1HYWgrbUJrJCmLet38VJl2rWQyhdY/BzEUVLYj9E=\n",
  "iv" => "xOLYEz+5Q18BMyJT\n",
  "auth_tag" => "8YdIeMr6p134PC2Jx8Mxtw==\n",
  "version" => 3,
  "cipher" => "aes-256-gcm"
  },
  "key" => {
    "encrypted_data" =>
      "W/7bnnBX9hEQacNfphoQ++hMwunkh9QNkU/kS50m3lt3GJ7PZGjt11Ie8Tej\n" \
      "/2YpAcfjiA3YyiVRCgbqQzSjqxYuzpXfWX8/nKeH2kbTjf3GejYknS+4/B4V\n" \
      "k56nqLPOkbA61ta8n+M2XGXxqrAcE5aSfOBWvonWyKzPT8yjZEjmFmMPBW3h\n" \
      "ZhyIQ3+U/qqx2l6UYAo5msvxEXx4xq282kYXlhE4QGNYM+dh/dbW3KAMtNzO\n" \
      "mSo5NTBbtwdDFtPtvxEFiBEnFM1E8APlCg4oGlS9lJIUGaWYaP98uylS9k7F\n" \
      "MdSO5wCqeKEowEzjmKfjo5Zq5Au8zUpxOqTPOU9cUmWkzTs3YtE8DbI80uqe\n" \
      "9Ab3TgO2XBEn2iP0XnCWf8MS+u+zNaB2Lmys4OaL2UyrukmcoEUB2AxLrHpF\n" \
      "+VOxi8d4r5PZJu9PORFsIZ6e3fF1LPstzv2cMR0LVf42VM2ih0kCwj8TPJ1u\n" \
      "FKcld3tKDknKH/BWrc1NRn+S8CCkDP++C1mmlrlSDM6WS6kIgAMgamvwOlIH\n" \
      "Kc3ouDaqTs7DAaJP2epYNijK8dvwcZqkfsEIs31dmuNwnh3t0NdlIbA03RK5\n" \
      "0ZQxlgsQfVtvD//Alf1b4Gw054Vc3uH96rCsr+TNy2fPhXCP/79utAtYT/+A\n" \
      "T7VmNDfFYyLhiiXUV8RK07BAjHrWSmssWKG/VWqLDVRpnNk5AeLtcp053m6s\n" \
      "xKTSqzSwg2vTLFSNIJjSn6AOYN6CEBRGkFKgYP4pekqHjYx7pT6p/6QQCJsO\n" \
      "vTv07KBX7i+6rLoFC9h27oxY1Wg5nbLaludseZI8WrXBP5uXt17gdsi+dgvl\n" \
      "GRkvGFI8XwXcJmoRip8q4Vt0n9kF8cFD8ANt3DLu4o6D8J0fmYkMbowu13GQ\n" \
      "IGDdx6VqU/+oCgccoxN/tWOXs/1U9hY96DNKuAQGoAVNmZpoCn1A+lS8wDjc\n" \
      "tKN28fdEseiGRY/HnlAw+Bg08y+G4zocHvnVatZP9rNckKcOVRIUtEGs6PZb\n" \
      "9EXK3MyQBWPZ1AYStJjup/VocqA7RbjIUiXiWfxbvd9s6DG3qgj38Bz7XkV6\n" \
      "9ihGpxInTeyNXb9o1E+2/4DQItw+7IwZcHWbJi6JMuDN1ecyGTNVtS+qSJ5X\n" \
      "0H5JFjl6s67hA+sjI2WuEwlJ6KlHRGM4mi5q0+h+dUp0qjXxDG6S4kK/n/Yt\n" \
      "hid+687t1ud+BZ/2430wx0u7i5NHET7JemuXD+fMhGZbTob04yDQbKwnO7H9\n" \
      "9PpBNPLNsH/+Iq465f5tL1/pjTddpl2lnXq6ZPRp8XeUHZNeFhUfG7anw6F7\n" \
      "4LP0629rz8SLrBleAYMemuPUVt2HxhaKTCFQ2CYHG5aVOvwjDowt6QVht2wm\n" \
      "kBTBPEpAjfTHVptgjHE45RX02gYtu6uekHQBPxaMyg259kN7yWyOYUnUFR7d\n" \
      "ON/BL7phB371zq3OvKmFKIr5DdlaTiPGuRvCk9wZ5r+2gPfS9HWXHcaW/UbZ\n" \
      "aA1OPAKbnNA0HGKas6rrjxFF3reA1Da/HFIKVUrLtMEPewV1hGCDvxF/EwsY\n" \
      "BHIX02I+iuR5Bem52EdiNnRUo42K5wcs7bbfdBkNc210vVECzbETvLgvjeBS\n" \
      "kkoDnaj58Eke560oZOnI4FC0K7WSUrbTWW0YsaWR8NYinn7uLuKD7relvFp/\n" \
      "x4SR8X7Rv1Wai7BcJc42fKKC/oMzNqF7ZSeGhZJ+XfzKEapzudrOrMWFfhWW\n" \
      "UFKkUzV1bC0/L0U2F0xz2lWHrYSAtq28WcP8l++HG1fsZdxLTx1h8YXbIasj\n" \
      "J6s5j1c1TnVcaXhcXBtdSgk5eBIDlHuIx6kGYmCUC1urK3N6JOrVpt7DlN1L\n" \
      "TdTnuQLA+Gr6KCW7akZlCAwZ/m+DkmiV+cQfLpT6fkaOUMWh4AYCCtWyF5VG\n" \
      "hdXopbEY6GDP/m/v1iariSYmMHQUyU3uYHpdYewEmTE2MpwDVirCRBtGyUve\n" \
      "8ndBw0dMrem31lYRmoj4dqcAHLjTLr0Yr4OTfefF2igEesFswP+NEUSvUMmA\n" \
      "SzfjuHCFSKPfvs6kT9GTjGLITBm0GPKVE7T6sEyCzYrV/lB+0xndl/B7evCh\n" \
      "ukv7Cy6FmIG/fo2wJf/MvHnXntT+q/pnRu0AcGMHXVax9uOq+n8kIjSkdxdu\n" \
      "T7nhKqs9xy5N041epT5HQPTAfFmBlMQgjL/rvr5NXvkqS0Y7sz1wCIDGcpC7\n" \
      "wdKXsqwxoVTw9CP1bWPLwuxGq3xyE/dKRsgKWQtO8jpe6nvLDS4hHX7kBqNk\n" \
      "tH8eD+KmLHGCYgYRl145JCO+RsM/5Z1WxHiBd3yn+4LVVAWfOTodeEIB9mcm\n" \
      "bxyJWxplsFRh5wl65SJYJ+3Mw6U0A0Ya12lXpIinO3bqmzaji8kIgId1FGsD\n" \
      "VBdCzrKOvYzgMfUVpj3memN8UJKoH0msT3gfRwr6vLfvXFJUpQF9aBmBh8f6\n" \
      "rCw7L5+PyNvZTGQ8KUCpvE84+rHFaD1mkiLouM2v23qHr3gU9SVkMPEePL2x\n" \
      "dokggCMukyFKIIBUx2CrS5Mbjwy+xnACnNAsCyNej5goQcLdaB6YyA5X/UaT\n" \
      "2WV3nO3JD8dlM+/vKI99CUKbr/O0OrFRSdxZxLjTv7AcM9pTdL3D5CkbtN67\n" \
      "PfUadiJHwQ6TJuT3hCITliFWu6fVj7HPDNgZPwRKcg3NVf8g1A10oPj5JvFT\n" \
      "cdKpclD+UnNkD3A7XmDIRQAinsHyTS/6AkC5/hMpw4MpApjnDaDb27anKuMO\n" \
      "8YRH5gdbD5l4Q8VFOeJuM7wCGSFshGjaGZNGnGu4HGW7nCTrr0P8g9mb4qFP\n" \
      "6l1W0irE+fhfQOSYA9nv132Y1DwVK40Jb/PFQcKilwZsrk/rR1X4t/TKRzCE\n" \
      "Yn8YQtJG2t2EUD0sdGOoTUIjEa1NzxAh2azaY1+aFo0KdyF0fKydgT2Ro35O\n" \
      "i9hfxpFvy2jYYOyWp7rR7EVcby3XWeLV2Ek/QpG2EH+YYKE2DIeX28kKTpOo\n" \
      "kOwhmfoE+RNTHNPyzSrvXBGBgraDyxVufydFDiyu0FO30+68/6yjMkihR5F7\n" \
      "Zp0ygULy0PfOAQHTj1yfqLu5aCxi9KAIgOHIZRVO0QQcZ6NSG4tlEC6vHuJ1\n" \
      "dAkzwot/MO6MHBQYSfL0k7d9GQpCKp3KkSUD9Wulf0WN4OGwdb18EdXU/cDy\n" \
      "+woh3IcoZzVFKOQfobb0LEYKTTnJckgOS/+f8jQOibExkpy4vdjxtpJkNDCr\n" \
      "qSp7KSh/FbCo6TEg0qNtVSf5Jo+GmaXalhtI+uxmsWyWGFBpe4/Mi5uvfY19\n" \
      "kNq68UGfiiYJESLt8hlGoTd7SBidlI6hnBaVraDN6PvXNdf6+AZIC4aj+Y6P\n" \
      "xAcTxfJDXkm3eqRKcHRKsxvOcjTexVy2wgsY0pdJOTuY4VxrGUzRZg6ZEYJn\n" \
      "7QWUQAF3uKpoSc5snUyGPGG6Jxz2+P6HANX48iynGmoK5gXNB4eVe3nW56K6\n" \
      "5Y7M2c5qRtgNcj+EnNYRxYsIYFYtpxDfLxSXTJ/nlXkgXWliISCZS5fnvIR5\n" \
      "AHtJSe8ZAiUi60lvR7fH6c1PJi/grrT5E0he+MNIP4yj78A4R+RRiNUQzB9D\n" \
      "DHVVBuUQrtsqq/XqpS0+5F30kBOxe8175nQPhVbAb+BxG0ixSiP5OO4BuNEj\n" \
      "xh0dRhPNrSYqcvv+tFRO9BNbbv7rsTvMc5AU3hJF1XDlROe9APA1AtaqPJtf\n" \
      "f5fQd8m2KDCqyNv0am1ZmqZCx4r6XMeit/R6SVRCzqjdWd7chyQWX+KJJbA3\n" \
      "GEVjJ+1tYr/NtXbcrpN22tFl047NyJlcYCs+x2dCgZEGCoSqpluaSAWJQAjF\n" \
      "HWccqvwlhj2Ty2FeaXvY3HQVMQkM34rqP1MazM8jrs+GGM2bg3gSizsHn9OH\n" \
      "v6C5q14SKNr9yiROiWAFN3lt9+p4NCHHf36+tUUqbRjGCxqdj91q28n3DAA+\n" \
      "mBclWAegi8Grqnyjic7f79jvu27t5l7xHxHmgv0UWMXB1MRIVGLHfvBgSI+0\n" \
      "VERelvWTm1zVxdlhgWOOkHPA0V/kjfXb5JIRa7SXICfv1FKm/Xxai6aqo5sH\n" \
      "/N3ZcpObMaBxOwo6eMHunlITP+6WHDIvuWFuT+LxvQw5lY7sAWP1L3vbm+7h\n" \
      "xmmeAKnfz13wJCRgQiIhtMfXvl7u30lYRHY7EWaPUupc48q/4Sx4No0YLE5k\n" \
      "tIfZmlWJSKUWJY9axAVRKIpifGha5k2JUxYmMJxu7TWjC2n7sk3W7KgdsdPq\n" \
      "iPMbQQ9mc91ZQvKjZyBj7hf8IAa6C2wwIhUpWj+Ci76Jg+SXHZgrVuls4b8Y\n" \
      "clJyRdOEDqq7IGOmp5legtzfjgdqFqskLaRQOPhjVZAYVLw6Ah5Y7xsX5hHk\n" \
      "uMF7EqYfF91rRz98MEuo10tdhyDYy1AtRyA=\n",
  "iv" => "swBxDaUC49vw/z8Y\n",
  "auth_tag" =>"6fHSUKwxIHTGRwQ8kSNY9w==\n",
  "version" => 3,
  "cipher" => "aes-256-gcm"
  }
}
