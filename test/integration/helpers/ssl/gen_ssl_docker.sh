#!/bin/bash

# Generate root certificate authority an public key
openssl genrsa -aes256 -out ca-key.pem 4096
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem

# Generate server/client key and certificate signing request
openssl genrsa -out key.pem 4096
openssl req -subj "/CN=localhost" -sha256 -new -key key.pem \
  -out cert.csr

# Add SAN
san="subjectAltName = IP:0.0.0.0,IP:127.0.0.1,"
san+="DNS:localhost,DNS:comap-swarm-manager-centos-7.kitchen,"
san+="DNS:comap-swarm-worker-centos-7.kitchen"
echo $san >> extfile.cnf

# Set extended usage attribute to be used for
# both server and client authentification
echo extendedKeyUsage = clientAuth,serverAuth >> extfile.cnf

# Generate the client/server signed certificate
openssl x509 -req -days 365 -sha256 -in cert.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out cert.pem -extfile extfile.cnf

# Remove certificate signing request and external configuration file
rm cert.csr extfile.cnf
