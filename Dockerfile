FROM ruby:2.5.0-alpine

RUN apk update && \
    apk upgrade && \
    apk add build-base git && \
    rm -rf /var/cache/apk/*

RUN mkdir /usr/app
WORKDIR /usr/app

COPY ./pkg/comap*.gem /usr/app/comap.gem

RUN gem install comap.gem

ENTRYPOINT ["/usr/local/bundle/bin/comap"]
