# frozen_string_literal: true

name 'comap-test'
maintainer 'SRE Gems'
maintainer_email 'incoming+sre-gems/comap@incoming.gitlab.com'
license 'Apache-2.0'
description 'Integration tests for Comap'
long_description 'Integration tests for Comap'
source_url 'https://gitlab.com/sre-gems/comap'
issues_url 'https://gitlab.com/sre-gems/comap/issues'
version '1.0.0'

chef_version '>= 13.0'

supports 'centos', '>= 7.4'

depends 'docker-platform'
