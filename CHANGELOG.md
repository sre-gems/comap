Changelog
=========

1.2.0
-----

Main:

- feat: add service label to metrics

Tests:

- test: do not get ci conf from test cookbook
- test: use an extra dns resolver for swarm insiders
- test: add ssl script helper to gen docker pki
- test: update ssl cert with kitchen tld

Misc:

- style: fix rubocop offenses (add line after guard)
- chore: use latest dependencies (mostly for test)

1.1.0
-----

Main:

- feat: enable compression with Rack::Deflater
- fix: handle spaces in labels
- feat: handle service in global mode

Tests:

- test: include .gitlab-ci.yml from test-cookbook
- test: use a tmpfs for internal /var/lib/docker
- test: build comap image with network host
- test: update test dependencies

Misc:

- chore: set generic maintainer & helpdesk email
- doc: fix default port in README
- style(rubocop): fix expand\_path offense
- doc: use doc in git message instead of docs

1.0.0
-----

- Initial version with Docker Swarm support
