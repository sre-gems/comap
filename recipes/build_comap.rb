# frozen_string_literal: true

# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Build comap docker image and launch it
directory '/root/pkg'

cookbook_file '/root/Dockerfile' do
  source 'Dockerfile'
  notifies :run, 'execute[docker build comap]', :delayed
end

cookbook_file '/root/pkg/comap.gem' do
  source 'comap.gem'
  notifies :run, 'execute[docker build comap]', :delayed
end

execute 'docker build comap' do
  command 'docker build --network host . -t makeorg/comap-for-kitchen-test'
  cwd '/root'
  action :nothing
end
