# frozen_string_literal: true

# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Launch dns service
docker_image 'andyshinn/dnsmasq'

docker_container 'dnsmasq' do
  repo 'andyshinn/dnsmasq'
  network_mode 'host'
  cap_add ['NET_ADMIN']
  restart_policy 'unless-stopped'
  subscribes :redeploy, 'docker_image[andyshinn/dnsmasq]', :immediately
end

# Create secrets
dir = '/root/.docker'
%w[ca.pem cert.pem key.pem].each do |secret|
  execute "docker secret create #{secret} #{dir}/#{secret}" do
    not_if "docker secret ls | grep #{secret}"
  end
end

metrics_content = <<~CONTENT
  # TYPE metrics_sample_seconds gauge
  metrics_sample_seconds 42
  # TYPE metrics_sample_2_seconds gauge
  metrics_sample_2_seconds 24
CONTENT

docker_platform_service 'metrics-replicated' do
  options(
    mode: 'replicated',
    replicas: 2,
    network: 'kitchen',
    publish: '5678:5678'
  )
  image 'hashicorp/http-echo'
  command "-text '#{metrics_content.chomp("\n")}'"
  action :create
end

docker_platform_service 'metrics-global' do
  options(
    mode: 'global',
    network: 'kitchen',
    publish: '5679:5678'
  )
  image 'hashicorp/http-echo'
  command "-text '#{metrics_content.chomp("\n")}'"
  action :create
end

server = 'https://comap-swarm-manager-centos-7.kitchen'
ssl = {
  'client_cert' => '/run/secrets/cert.pem',
  'client_key' => '/run/secrets/key.pem',
  'ca_file' => '/run/secrets/ca.pem'
}
ssl_opt = ssl.map { |k, v| "#{k}=#{v}" }.join(',')
{
  'replicated' => '9397',
  'global' => '9398'
}.each_pair do |mode, port|
  docker_platform_service "comap-#{mode}" do
    options(
      mode: 'replicated',
      network: 'kitchen',
      dns: node['ipaddress'],
      publish: "#{port}:9397",
      secret: %w[cert.pem key.pem ca.pem]
    )
    image 'makeorg/comap-for-kitchen-test'
    command "-H #{server} -p 2376 -s #{ssl_opt} "\
            "-n kitchen metrics-#{mode}:5678"
    action :create
  end
end
