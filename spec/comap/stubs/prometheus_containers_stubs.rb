# frozen_string_literal: true

# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

metrics = (1..3).map do |i|
  <<~BODY
    # HELP metrics_sample_seconds help message
    # TYPE metrics_sample_seconds gauge
    metrics_sample_seconds #{i}
    # HELP metrics_timestamped_sample_seconds help message
    # TYPE metrics_timestamped_sample_seconds gauge
    metrics_timestamped_sample_seconds #{i} 151852813#{i}
    # TYPE metrics_labeled_sample_seconds gauge
    metrics_labeled_sample_seconds{label="test#{i}"} #{i}
    # TYPE metrics_labeled_with_space_sample_seconds gauge
    metrics_labeled_sample_seconds{label="test #{i}"} #{i}
  BODY
end

RSpec.configure do |config|
  config.before(:each) do
    (1..3).each do |i|
      url = "http://10.0.0.#{i}:4000"
      stub_request(:get, url)
        .to_return(status: 200, body: metrics[i - 1], headers: {})
      stub_request(:get, "#{url}/metrics")
        .to_return(status: 200, body: metrics[i - 1], headers: {})
    end
  end
end
