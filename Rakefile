# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'bundler/gem_tasks'
require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) { |task| task.verbose = false }

task default: :spec

desc 'Prepare files directory for kitchen tests'
task kitchen: :build do
  mkdir_p('files')
  version = if COMAP::VERSION.include?('-')
              COMAP::VERSION.split('-').insert(1, 'pre').join('.')
            else COMAP::VERSION
            end
  cp("pkg/comap-#{version}.gem", 'files/comap.gem', preserve: false)
  cp('Dockerfile', 'files/.', preserve: false)
end
CLEAN.include('files', 'pkg')
