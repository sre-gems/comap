# frozen_string_literal: true

# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'faraday'
require 'openssl'

# Main module
module COMAP
  # Rack App
  class App # rubocop:disable Metrics/ClassLength
    # rubocop:disable  Metrics/ParameterLists
    def initialize(
      docker_host: 'http://127.0.0.1',
      docker_port: '2375',
      docker_ssl: '',
      metrics_path: '',
      network: '',
      services: []
    )
      raise ArgumentError, 'No services given' if services.empty?
      raise ArgumentError, 'No network given' if network.nil?

      ssl_hash = parse_ssl(docker_ssl)
      @client = DockerClient.new(docker_host, docker_port, ssl_hash)
      @network = network
      @services = services
      @metrics_path = metrics_path
      error_msg = 'Could not retrieve Docker API version from '\
        "#{docker_host}:#{docker_port} ssl:#{docker_ssl}"
      raise StandardError, error_msg if @client.call('version').nil?
    end
    # rubocop:enable  Metrics/ParameterLists

    def call(_env)
      ['200', { 'Content-Type' => 'text/plain' }, [body]]
    end

    private

    def body
      @services.map do |s|
        service, port = s.split(':')
        service_metrics(service, port)
      end.compact.map(&flat_hash).join
    end

    def service_metrics(service, port)
      containers(service).sort_by { |c| c['slot'] }.map do |container|
        next puts "#{service} service not found" if container.empty?

        url = "http://#{container['ip']}:#{port}/#{@metrics_path}"
        container_metrics(url, service, container['slot'])
      end.reduce(&merge_proc)
    end

    def container_metrics(url, service, slot)
      data = scrape_metrics(url, service)
      return {} if data.nil?

      data.lines.each_with_object({}) do |value, hash|
        next hash[value] = nil if value.start_with?('#')

        (hash[hash.keys.last] ||= []) << add_label(value, service, slot)
      end.compact
    end

    def scrape_metrics(url, service)
      Faraday.get(url) do |req|
        req.options.timeout = 2
        req.options.open_timeout = 1
      end.body
    rescue StandardError
      puts "Could not connect to #{service} endpoint #{url}"
    end

    def add_label(line, service, slot)
      replace = %(service="#{service}",container="#{service}.#{slot}")
      if line[/{.*?\}/].nil?
        line.sub(/ /, "{#{replace}} ")
      else
        line.sub(/(\{(.*)\})/, "{\\2,#{replace}}")
      end
    end

    def containers(service)
      running_tasks(service).map do |task|
        {
          'slot' => task['Slot'] || task['NodeID'],
          'ip' => ip(task).split('/').first
        }
      end.uniq
    end

    def running_tasks(service)
      filters = {
        'service' => { service => true },
        'desired-state' => { 'running' => true }
      }
      @client.call('tasks', filters).keep_if do |task|
        task['Status']['State'] == 'running' if task.is_a?(Hash)
      end
    end

    def ip(task)
      task['NetworksAttachments'].select do |h|
        h['Network']['Spec']['Name'] == @network
      end.first['Addresses'].first
    end

    # helpers

    def parse_ssl(docker_ssl)
      docker_ssl.split(',').map do |opt|
        k, v = opt.split('=')
        if k.include?('cert')
          v = OpenSSL::X509::Certificate.new(File.read(v))
        elsif k.include? 'key'
          v = OpenSSL::PKey::RSA.new(File.read(v))
        end
        [k, v]
      end.to_h
    end

    def merge_proc
      proc do |old, new|
        old.merge(new) { |_, value1, value2| value1 + value2 }
      end
    end

    def flat_hash
      proc do |item|
        item.map { |key, value| "#{key}#{value.join}" }
      end
    end
  end
end
